# README #

### The News Flow Project ###

* The project consists of two parts: a "Mock News Feed" and a "News Analyser". Several instances of the Mock News Feed can be run simultaneously, each connecting to the same News Analyser.
* The Mock News Feed should periodically generate messages containing random News Item.
* The News Analyser should handle the messages from the news feeds and periodically display a short summary about news items that are considered "interesting".

### Mock News Feed ###
The current repository is for the Mock News Feed part of the project.

* Creates a persistent TCP connection to the News Analyser and periodically sends news items over that connection. Each news item should be comprised of a headline and a priority.
* The headline of a news item should be a random combination of three to five words from the following list: up, down, rise, fall, good, bad, success, failure, high, low, über, unter.
* The priority of a news item should be an integer within the range [0..9]. News messages with higher priority should be generated with less probability than those with lower priority.
* The frequency of news items being emitted by the feed should be configurable via a Java property.

### Setup  ###

* The project consists of two repositories: Mock News Feed and News Analyser. Each of them is a Spring Boot project on Gradle.
* They can be run by opening in an IDE or directly via Gradle build tool from the console. [Official Gradle documentation](https://docs.gradle.org).
* The application properties can be found by the following path:  
`
<project root>/src/main/resources/application.properties
`
* The port of the TCP server is `3000` by default - can be easily changed onto an appropriate one in the properties files. TCP connection was established via [Spring Integration](https://docs.spring.io/spring-integration/docs/5.3.1.RELEASE/reference/html/).
