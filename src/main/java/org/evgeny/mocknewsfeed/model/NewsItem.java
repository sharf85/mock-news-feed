package org.evgeny.mocknewsfeed.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

// The class combines the functionalities of an entity and of a dto, which is not good.
// But in this particular case it is justified in order not to over complicate the app even more.
// By the same reason, the fields are completely open here.
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NewsItem {

    /**
     * An integer value from the range [0..9]
     */
    int priority;

    /**
     * A set of words
     */
    Set<String> headline;

}
