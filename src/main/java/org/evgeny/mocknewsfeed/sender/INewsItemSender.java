package org.evgeny.mocknewsfeed.sender;

import org.evgeny.mocknewsfeed.model.NewsItem;

/**
 * the tool used for sending News Items. In our case the only implementation of this
 * is the TCP implementation, configured in the {@link org.evgeny.mocknewsfeed.config.TcpConfig}
 */
public interface INewsItemSender {
    public void send(NewsItem item);
}
