package org.evgeny.mocknewsfeed;

import org.evgeny.mocknewsfeed.generator.INewsItemGenerator;
import org.evgeny.mocknewsfeed.model.NewsItem;
import org.evgeny.mocknewsfeed.sender.INewsItemSender;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableScheduling
public class MockNewsFeedApplication {
    public static void main(String[] args) {
        SpringApplication.run(MockNewsFeedApplication.class, args);
    }

    final private INewsItemSender sender;
    final private INewsItemGenerator newsItemGenerator;

    public MockNewsFeedApplication(INewsItemSender sender, INewsItemGenerator newsItemGenerator) {
        this.sender = sender;
        this.newsItemGenerator = newsItemGenerator;
    }

    @Scheduled(fixedRateString = "${org.evgeny.mocknewsfeed.frequency}")
    public void feedNext() {
        NewsItem newsItem = newsItemGenerator.generate();
        sender.send(newsItem);
    }
}
