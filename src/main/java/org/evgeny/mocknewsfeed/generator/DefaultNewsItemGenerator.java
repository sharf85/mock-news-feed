package org.evgeny.mocknewsfeed.generator;

import org.evgeny.mocknewsfeed.model.NewsItem;
import org.evgeny.mocknewsfeed.util.RandomItem;
import org.evgeny.mocknewsfeed.util.RandomItemGenerator;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * The default  implementation of the News Generator.
 * The headline of a news item should be a random combination of three to five words from the
 * following list: up, down, rise, fall, good, bad, success, failure, high, low, über, unter.
 * The priority of a news item should be an integer within the range [0..9]. News messages
 * with higher priority should be generated with less probability than those with lower priority.
 */
public class DefaultNewsItemGenerator implements INewsItemGenerator {

    int minWordsNum;
    int maxWordsNum;
    Set<String> words;
    List<Double> priorityProbabilities;

    RandomItemGenerator<Integer> priorityGenerator;
    RandomItemGenerator<String> wordGenerator;
    Random random = new Random();

    /**
     * @param words                 which participate in generating messages
     * @param priorityProbabilities - relative probabilities of the priorities, where the index is a
     *                              priority and the value is a probability of the priority
     */
    public DefaultNewsItemGenerator(
            Collection<String> words,
            Collection<Double> priorityProbabilities,
            int minWordsNum,
            int maxWordsNum) {
        this.words = new HashSet<>(words);
        this.priorityProbabilities = new ArrayList<>(priorityProbabilities);
        this.minWordsNum = minWordsNum;
        this.maxWordsNum = maxWordsNum;
    }

    /**
     * The method should be called after a constructor call. Initializes the generators of words
     * and probabilities under the hood.
     */
    public void init() {
        initPriorityGenerator();
        initWordsGenerator();
    }

    void initWordsGenerator() {
        List<RandomItem<String>> randomWordItems = words.stream()
                .map(word -> new RandomItem<>(word, 1))
                .collect(Collectors.toList());
        this.wordGenerator = new RandomItemGenerator<>(randomWordItems);
    }

    void initPriorityGenerator() {
        List<RandomItem<Integer>> randomPriorityItems = IntStream.range(0, priorityProbabilities.size())
                .mapToObj(index -> new RandomItem<>(index, priorityProbabilities.get(index)))
                .collect(Collectors.toList());
        this.priorityGenerator = new RandomItemGenerator<>(randomPriorityItems);
    }

    @Override
    public NewsItem generate() {
        int priority = priorityGenerator.next().getItem();
        int nWords = minWordsNum + random.nextInt(maxWordsNum - minWordsNum + 1);

        Set<String> words = new HashSet<>();
        while (words.size() < nWords) {
            String nextWord = wordGenerator.next().getItem();
            words.add(nextWord);
        }
        return new NewsItem(priority, words);
    }
}
