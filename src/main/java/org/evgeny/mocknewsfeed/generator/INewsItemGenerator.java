package org.evgeny.mocknewsfeed.generator;

import org.evgeny.mocknewsfeed.model.NewsItem;

/**
 * The classes inheriting this interface, must implement a way to generate new messages to send
 */
public interface INewsItemGenerator {
    NewsItem generate();
}
