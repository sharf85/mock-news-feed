package org.evgeny.mocknewsfeed.config;

import org.evgeny.mocknewsfeed.generator.DefaultNewsItemGenerator;
import org.evgeny.mocknewsfeed.generator.INewsItemGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class AppConfig {

    @Value("#{'${org.evgeny.mocknewsfeed.news.default.words}'.split(',')}")
    private List<String> words;

    @Value("#{'${org.evgeny.mocknewsfeed.news.default.priorities}'.split(',')}")
    private List<Double> priorityProbabilities;

    /**
     * @return an instance of NewsItemGenerator used in the project
     */
    @Bean
    public INewsItemGenerator defaultNewsItemGenerator() {
        DefaultNewsItemGenerator itemGenerator = new DefaultNewsItemGenerator(
                words,
                priorityProbabilities,
                3,
                5
        );
        // Here could be a factory to instantiate a DefaultNewsItemGenerator instance,
        // in this case the init() method should be package private and used in this factory.
        // But this is a matter of refactoring.
        itemGenerator.init();
        return itemGenerator;
    }
}
