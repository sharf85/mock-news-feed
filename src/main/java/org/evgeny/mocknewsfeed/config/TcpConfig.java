package org.evgeny.mocknewsfeed.config;

import org.evgeny.mocknewsfeed.model.NewsItem;
import org.evgeny.mocknewsfeed.sender.INewsItemSender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Transformers;
import org.springframework.integration.ip.dsl.Tcp;
import org.springframework.integration.ip.tcp.connection.AbstractClientConnectionFactory;
import org.springframework.integration.ip.tcp.connection.TcpNetClientConnectionFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;

@Configuration
@EnableIntegration
public class TcpConfig {

    /**
     * Connection settings for the tcp client. Uses a permanent {@link java.net.Socket} to send data
     * under the hood
     * @param host of the tcp server
     * @param port of the tcp server
     * @return a factory bean used in the spring IntegrationFlow from below
     */
    @Bean
    public TcpNetClientConnectionFactory tcpConnection(
            @Value("${org.evgeny.mocknewsfeed.tcp.server.host}") String host,
            @Value("${org.evgeny.mocknewsfeed.tcp.server.port}") int port) {
        return new TcpNetClientConnectionFactory(host, port);
    }

    /**
     *
     * @param tcpConnectionFactory - the connection factory bean from above
     * @param toTcp the MessageChannel used in the flow, as a source of messages for sending
     *              to the tcp server
     * @return a bean describing the messaging flow via tcp
     */
    @Bean
    public IntegrationFlow tcpClientFlow(AbstractClientConnectionFactory tcpConnectionFactory,
                                 MessageChannel toTcp) {
        return IntegrationFlows.from(toTcp)
                .transform(Transformers.toJson())
                .handle(Tcp.outboundAdapter(tcpConnectionFactory))
                .get();
    }

    /**
     * @return the channel which is the source for messaging via tcp
     */
    @Bean
    MessageChannel toTcp() {
        return new DirectChannel();
    }

    /**
     * @param toTcp - the bean from above
     * @return a bean of {@link INewsItemSender}. The bean is used in the business logic to send
     * NewsItems
     */
    @Bean
    public INewsItemSender tcpNewsItemSender(MessageChannel toTcp) {
        return item -> {
            Message<NewsItem> message = new GenericMessage<>(item);
            toTcp.send(message);
        };
    }

}
