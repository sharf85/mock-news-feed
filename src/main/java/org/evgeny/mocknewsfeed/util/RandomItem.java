package org.evgeny.mocknewsfeed.util;


public class RandomItem<T> {
    static final String NEGATIVE_RELATIVE_PROBABILITY = "Relative probability must be positive";
    T item;
    double relativeProbability;

    /**
     * @param item                the item which is produced by {@link RandomItemGenerator}
     * @param relativeProbability is not a probability, strictly speaking. It is the value defining
     *                            how often the item should be taken relative to the other items.
     */
    public RandomItem(T item, double relativeProbability) {
        this.item = item;
        if (relativeProbability <= 0)
            throw new WrongRandomItemException(NEGATIVE_RELATIVE_PROBABILITY);
        this.relativeProbability = relativeProbability;
    }

    public T getItem() {
        return item;
    }

    public double getRelativeProbability() {
        return relativeProbability;
    }
}
