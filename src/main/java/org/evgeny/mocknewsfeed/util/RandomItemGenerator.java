package org.evgeny.mocknewsfeed.util;

import java.util.*;

/**
 * The class is providing one of the random items, based on their relative probabilities.
 * For instance, there are three items: [(item1, 3.5), (item2| 2.5), (item3| 1)], then
 * the instance of the class will produce the first item with the probability of 1/2 = 3.5/(3.5+2.5+1),
 * the second item: 5/14 = 2.5/(3.5+2.5+1), and the third item: 1/7 = 1/(3.5+2.5+1)
 * via the next() method
 *
 * @param <T>
 */
public class RandomItemGenerator<T> {

    List<RandomItem<T>> items;
    double sum;
    Random random;
    /**
     * distribution is the list of double values corresponding to each item.
     * If there are three items: [(item1, 3.5), (item2| 2.5), (item3| 1)],
     * then the distribution will be [3.5, 6, 7]. Meaning, if we take a random number from 0 to 7,
     * it will be in the [0, 3.5) with the probability of 1/2, in the [3.5, 6) with the probability
     * 5/14 and in the [6, 7) with the probability of 1/7.
     */
    List<Double> distribution;

    public RandomItemGenerator(Collection<RandomItem<T>> items) {
        this.items = Collections.unmodifiableList(new ArrayList<>(items));
        this.sum = this.items.stream()
                .reduce(0d,
                        (prev, item2) -> prev + item2.relativeProbability,
                        Double::sum);
        this.distribution = collectDistribution();
        this.random = new Random();
    }

    List<Double> collectDistribution() {
        List<Double> res = new ArrayList<>();
        double currentValue = 0;
        for (RandomItem<T> item : items) {
            currentValue += item.relativeProbability;
            res.add(currentValue);
        }
        return res;
    }

    /**
     * The method performs as follows. First, a random number from the interval [0, sum)
     * is taken. Sum - is the sum of all the relative probabilities of the items.
     * After that, the binary search algorithm determines between which of the values
     * from 'distribution' list the random number is. According to the index, the corresponding
     * {@link RandomItem} returns. The complexity of the method is O(log(N)), where N is
     * the number of random items. It is reasonable, of course, when N is at least 50-100.
     * In our case, when we have not so big choice of priorities and words for messages,
     * it is not that useful.
     *
     * @return a random item according to its relative probability
     */
    public RandomItem<T> next() {
        // the method is based on the binary search approach over the 'distribution' object
        double nextDouble = random.nextDouble() * sum;
        int idx = Collections.binarySearch(distribution, nextDouble);
        idx = idx >= 0 ? idx + 1 : -idx - 1;
        return items.get(idx);
    }

    public List<RandomItem<T>> getItems() {
        return items;
    }

}
