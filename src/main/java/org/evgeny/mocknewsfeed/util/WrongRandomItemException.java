package org.evgeny.mocknewsfeed.util;

public class WrongRandomItemException extends RuntimeException {
    public WrongRandomItemException(String message) {
        super(message);
    }
}
