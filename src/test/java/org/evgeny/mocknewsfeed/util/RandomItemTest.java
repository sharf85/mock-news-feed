package org.evgeny.mocknewsfeed.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RandomItemTest {

    @Test()
    public void testConstructor_negativeRelativeProbability_throwsException() {
        assertThrows(WrongRandomItemException.class, () -> new RandomItem<>("", -0.01));
    }

    @Test()
    public void testConstructor_zeroRelativeProbability_throwsException() {
        assertThrows(WrongRandomItemException.class, () -> new RandomItem<>("", 0));
    }
}
