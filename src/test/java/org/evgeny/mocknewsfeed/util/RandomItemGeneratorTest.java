package org.evgeny.mocknewsfeed.util;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class RandomItemGeneratorTest {

    RandomItemGenerator<Integer> generator;

    @Test
    public void testConstructor_3integerItems_properlyInitialized() {
        Collection<RandomItem<Integer>> items = Arrays.asList(
                new RandomItem<>(0, 5),
                new RandomItem<>(1, 2),
                new RandomItem<>(2, 4)
        );
        generator = new RandomItemGenerator<>(items);

        assertEquals(11, generator.sum, 0.0001);
        assertEquals(Arrays.asList(5d, 7d, 11d), generator.distribution);
    }

    @Test
    public void testNext_3integerItems_deviationAsShould() {
        Collection<RandomItem<Integer>> items = Arrays.asList(
                new RandomItem<>(0, 5),
                new RandomItem<>(1, 2),
                new RandomItem<>(2, 4)
        );
        generator = new RandomItemGenerator<>(items);

        Map<Integer, Integer> qtyByItem = new HashMap<>();
        for (int i = 0; i < 100000; i++) {
            int item = generator.next().item;
            qtyByItem.computeIfPresent(item, (key, value) -> value + 1);
            qtyByItem.putIfAbsent(item, 1);
        }
// The probability of that the difference between the actual probability (5/11) and
// the frequency of occurrence of the '0' in 100000 independent trials, is larger
// than 0.01, is almost 0 (Corollary to the Laplace integral theorem.). That means,
// the assert should pass almost always. This also applies to the further asserts.
        assertEquals(5d / 11, qtyByItem.get(0) / 100000d, 0.01);
        assertEquals(2d / 11, qtyByItem.get(1) / 100000d, 0.01);
        assertEquals(4d / 11, qtyByItem.get(2) / 100000d, 0.01);
    }

}
