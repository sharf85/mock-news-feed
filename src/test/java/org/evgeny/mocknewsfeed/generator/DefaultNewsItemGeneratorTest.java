package org.evgeny.mocknewsfeed.generator;

import org.evgeny.mocknewsfeed.util.RandomItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DefaultNewsItemGeneratorTest {
    DefaultNewsItemGenerator generator;

    List<String> dictionary = Arrays.asList("one", "two", "three", "four", "five");
    List<Double> relativePriorityProbabilities = Arrays.asList(20d, 10d, 5d);
    final static int minWordsN = 3;
    final static int maxWordsN = 5;

    @BeforeEach
    public void init() {
        generator = new DefaultNewsItemGenerator(
                dictionary,
                relativePriorityProbabilities,
                minWordsN,
                maxWordsN
        );
        generator = spy(generator);
    }


    @Test
    public void testInit_callsInitWordsGeneratorAndInitPriorityGenerator() {
        generator.init();
        verify(generator, times(1)).initWordsGenerator();
        verify(generator, times(1)).initPriorityGenerator();
    }

    @Test
    public void testInitWordsGenerator_createsCorrectRandomItems() {
        generator.initWordsGenerator();
        List<RandomItem<String>> items = generator.wordGenerator.getItems();

        Set<String> expectenWords = new HashSet<>(dictionary);

        // tests the generated words
        assertEquals(
                expectenWords,
                items.stream()
                        .map(RandomItem::getItem)
                        .collect(Collectors.toSet())
        );

        // tests the generated relative probability of a word
        for (RandomItem<String> item : items) {
            assertEquals(1, item.getRelativeProbability(), 0.0001);
        }
    }

    @Test
    public void testInitPriorityGenerator_createsCorrectRandomItems() {
        generator.initPriorityGenerator();
        List<RandomItem<Integer>> items = generator.priorityGenerator.getItems();

        List<Integer> expectenPriorities = Arrays.asList(0, 1, 2);

        // tests the generated priority values
        assertEquals(
                expectenPriorities,
                items.stream()
                        .map(RandomItem::getItem)
                        .collect(Collectors.toList())
        );

        // tests the generated relative probabilities of the priorities
        assertEquals(
                relativePriorityProbabilities,
                items.stream()
                        .map(RandomItem::getRelativeProbability)
                        .collect(Collectors.toList())
        );
    }

}
