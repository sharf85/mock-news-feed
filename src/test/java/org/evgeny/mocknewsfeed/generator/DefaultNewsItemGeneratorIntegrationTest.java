package org.evgeny.mocknewsfeed.generator;

import org.evgeny.mocknewsfeed.model.NewsItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DefaultNewsItemGeneratorIntegrationTest {

    DefaultNewsItemGenerator generator;

    List<String> dictionary;
    List<Double> relativePriorityProbabilities;
    final static int minWordsN = 3;
    final static int maxWordsN = 5;
    List<Double> absolutePriorityProbabilities;


    @BeforeEach
    public void init() {
        dictionary = Arrays.asList("one", "two", "three", "four", "five", "six", "seven");
        relativePriorityProbabilities = Arrays.asList(20d, 10d, 5d);
        double sumPriorityProbabilities = relativePriorityProbabilities.stream()
                .mapToDouble(v -> v)
                .sum();
        absolutePriorityProbabilities = relativePriorityProbabilities
                .stream()
                .map(val -> val / sumPriorityProbabilities)
                .collect(Collectors.toList());


        generator = new DefaultNewsItemGenerator(
                dictionary,
                relativePriorityProbabilities,
                minWordsN,
                maxWordsN
        );
        generator.init();
    }

    @Test
    public void testGenerate_100000Messages_wordsEquallyProbableAndCorrectPriorities() {

        List<NewsItem> generatedItems = IntStream.range(0, 100000)
                .mapToObj(value -> generator.generate())
                .collect(Collectors.toList());

        // Count frequency of words
        Map<String, Long> qtyByWord = generatedItems.stream()
                .flatMap(newsItem -> newsItem.getHeadline().stream())
                .collect(groupingBy(word -> word, counting()));
        long qtyWords = generatedItems.stream().flatMap(item -> item.getHeadline().stream()).count();

        // check that all the words are equally probable
        assertWordFrequency(qtyByWord, qtyWords);


        // Count frequency of priorities
        Map<Integer, Long> qtyByPriorities = generatedItems.stream()
                .collect(groupingBy(NewsItem::getPriority, counting()));

        // check that the priorities appear according to their probability
        assertPriorityFrequency(qtyByPriorities);

        // count the quantities of generated words
        Map<Integer, Long> qtyByHeadlineSize = generatedItems.stream()
                .collect(groupingBy(item -> item.getHeadline().size(), counting()));

        // check that the size of the headlines is equally probable from the least to the most size.
        assertHeadlineSizeFrequency(qtyByHeadlineSize);

    }

    private void assertHeadlineSizeFrequency(Map<Integer, Long> qtyByHeadlineSize) {
        double expectedProbability = 1d / (maxWordsN - minWordsN + 1);

        for (int i = minWordsN; i <= maxWordsN; i++) {
            assertEquals(expectedProbability, qtyByHeadlineSize.get(i) / 100000d, 0.01);
        }

    }

    private void assertWordFrequency(Map<String, Long> qtyByWord, long qtyWords) {
        double expectedProbability = 1d / dictionary.size();
        for (String word : dictionary) {
            assertEquals(expectedProbability, qtyByWord.get(word) / (double) qtyWords, 0.01);
        }
    }

    private void assertPriorityFrequency(Map<Integer, Long> qtyByPriorities) {
        int size = absolutePriorityProbabilities.size();
        for (int i = 0; i < size; i++) {
            assertEquals(absolutePriorityProbabilities.get(i), qtyByPriorities.get(i) / 100000d, 0.01);
        }
    }
}
